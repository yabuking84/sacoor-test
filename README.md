# Backend Developer Challenge

### Please run this first. CLI command to create a test CSV file. (/project_root/app/Console/Commands/CreateCsv.php) 
- ```php artisan run:createtestcsv```.


### CLI command to convert the input CSV file to a JSON and XML file.
- ```php artisan run:convert```.


### REST API to serve the contents of the JSON file filterable by name and pvp. Serving a filtered response in XML format.
- http://localhost/api/get-xml-data.
- Filter payload should be name and pvp in POST data. ex. { "name": "back", "pvp": "2020" }.
- Filter payload can also be just name. ex. { "name": "back" }.
- Filter payload can also be just pvp. ex. { "name": "pvp" }.
- Filter payload can also be just blank to retrieve all.


### My solution and what can be improved.
- my main class is in App\Http\Modules\Convert. It has most of the functions there. Here you need to initialize Convert class to be able to use its features. 
- when CLI command to convert is run, it will convert test.csv (/project-root/storage/app/convert/test.csv) to JSON and XML. Two files (test.json and test.xml) will be created in directory (/project-root/storage/app/converted).
- the filterable REST API will have a response of xml.
- I have used ArrayToXml by Spatie to convert to XML.

### Docker
- run ```./vendor/bin/sail up```

### Tests should be included
- Tests are on the phpUnit.
- run ```php artisan test```.


#### By: Rene Rowell dela Rama

- [@Bitbucket](https://bitbucket.org/yabuking84/sacoor-test)
- rrsdr84@gmail.com
  