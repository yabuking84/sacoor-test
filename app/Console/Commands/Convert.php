<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Modules\Convert as Converter;

use Illuminate\Support\Facades\Storage;

class Convert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
   // protected $signature = 'command:name';
    protected $signature = 'run:convert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Converting CSV to JSON and XML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command. Converts a csv to json and xml files
     *
     * @return mixed
     */
    public function handle()
    {

        echo "\n\rConverting.. \n\r";

        $csv = Storage::disk('convert')->get('test.csv');
        $convert = new Converter($csv);
        
        $json = $convert->getJson();        
        $xml = $convert->getXml();
        
        
        Storage::disk('converted')->put('test.json',$json);
        Storage::disk('converted')->put('test.xml',$xml);

        echo "\n\rDone Converting! Check project_root/storage/app/public/converted directory. \n\r";
        echo "\n\r \n\r";

    }
}
