<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
   // protected $signature = 'command:name';
    protected $signature = 'run:createtestcsv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create test.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command. Converts a csv to json and xml files
     *
     * @return mixed
     */
    public function handle()
    {

        echo "\n\rCreating.. \n\r";



        $csv = 
'Id,name,sku,pvp,discount
123,Suit,S16123sdasds,2000,25
324,back,sdshj5,2020,78
543,front,sjhds5,2030,33
843,side,ewsbyssds5,2020,56
256,back,wochsds5,4020,34';

        Storage::disk('convert')->put('test.csv',$csv);

        echo "\n\rDone Creating! Check project_root/storage/app/convert directory. \n\r";
        echo "\n\r \n\r";

    }
}
