<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\ArrayToXml\ArrayToXml;

class FilterController extends Controller
{
  
    
    /**
     * API endpoint to get data from POST request.
     *
     * @param Request $rqst->name 
     * @param Request $rqst->pvp
     */    
    public function filterToXml(Request $rqst){

        $name = $rqst->name;
        $pvp = $rqst->pvp;

        $json = Storage::disk('converted')->get('test.json');

        $collection = collect(json_decode($json, true));
        $filtered  = $collection->filter(function ($value, $key) use ($name,$pvp) {
            if($value['name']==$name && !isset($pvp)) // if pvp is not set but name is found then return true 
            return true;
            else if($value['pvp']==$pvp && !isset($name)) // if name is not set but pvp is found then return true 
            return true;
            else if($value['name']==$name && $value['pvp']==$pvp) // if pvp and name is set and found then return true
            return true;
            else if(!isset($name) && !isset($pvp)) // if name and pvp is not set return true 
            return true;
            else 
            return false;
        });

        $filtered = ArrayToXml::convert(
            ['item'=>$filtered->toArray()], 
            [
                'rootElementName' => 'root',
            ], 
            true, 
            'UTF-8'
        );

		return response($filtered,200)
                ->header('Content-Type', 'text/xml');        

    }
}
