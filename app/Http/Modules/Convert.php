<?php

namespace App\Http\Modules;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;

use App\Exceptions\InvalidArrayFormatException;

use Spatie\ArrayToXml\ArrayToXml;

class Convert
{

    /**
     * The header title of the csv.
     *
     * @var array
     */
    private $headers;


    /**
     * The processed main_data to be easily converted to JSON/XML.
     *
     * @var array
     */
    private $main_data;











    /**
     * Initiate and process CSV to set headers, main_data.
     *
     * @param string $csv
     * @return void
     */
	public function __construct($csv){
        if ($csv) {
            $array = array_map('str_getcsv', explode(PHP_EOL, $csv));
            $this->headers = array_shift($array);
            $this->main_data = $this->processArray($array);

            // $json = $this->convertToJson($array);
            // Storage::disk('converted')->put('test-converted.json', $json);
        }
    }










    /**
     * Sets the proper key value pairs to be easily converted to JSON/XML.
     *
     * @param array $dataArray
     * @return array $formattedArray
     */
    private function processArray($dataArray){

        try {
            $formattedArray = array_map(array($this, 'mapArray'),$dataArray);
            $formattedArray = array_filter($formattedArray,array($this, 'filterArray'));
            return $formattedArray;
        } catch (InvalidArrayFormatException $exception) {
            report($exception);
            return $exception->getMessage()[0];
        }
    }









    /**
     * Private Function to be used by array_map in processArray()
     *
     * @param array $valArray
     * @return array
     */
    private function mapArray($valArray) {
        $headCnt = count($this->headers);
        $retVal = [];

        // will set as blank string if does not exist
        for($x=0;$x<$headCnt;$x++) {
            if(array_key_exists($x, $valArray) && $valArray[$x])
            $retVal[$this->headers[$x]] = $valArray[$x];
        }        

        // throws InvalidArrayFormatException if fields are not not equal. This can be commented so that it will just set it blank.
        // if($headCnt!=count($valArray)) 
        // throw new InvalidArrayFormatException("Invalid array format!");

        return $retVal;

    }








    /**
     * Private Function to be used by array_map in processArray(). Removes blank data.
     *
     * @param array $valArray
     * @return array
     */
    private function filterArray($valArray) {
        $retVal = false;
 
        if(count($valArray))
        $retVal = true;
         
        return $retVal;
    }
















    /**
     * Get JSON 
     *
     * @param void
     * @return string $retVal
     */
    public function getJson(){
        $retVal = Collection::make($this->main_data)->toJson(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return $retVal;        
    }






    /**
     * Get XML 
     *
     * @param void
     * @return string $retVal
     */
    public function getXml(){
        $retVal = ArrayToXml::convert(
            ['item'=>$this->main_data], 
            [
                'rootElementName' => 'root',
            ], 
            true, 
            'UTF-8'
        );

        return $retVal;
    }


}
