<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetXmlDataApiTest extends TestCase
{
    /**
     * Only test if name and pvp are set.
     *
     * @return void
     */
    public function test_can_retrieve_xml_by_name_pvp()
    {
        $formData = [
            'name' => "back",
            'pvp' => "2020"
        ];
        $response = $this->post(route('get_xml_data'),$formData);
        $response->assertStatus(200);
    }

    /**
     * Only test if only name and pvp is set.
     *
     * @return void
     */
    public function test_can_retrieve_xml_by_only_name()
    {
        $formData = [
            'name' => "back"
        ];
        $response = $this->post(route('get_xml_data'),$formData);
        $response->assertStatus(200);
    }    

    /**
     * Only test if only pvp is set.
     *
     * @return void
     */
    public function test_can_retrieve_xml_by_only_pvp()
    {
        $formData = [
            'pvp' => "2020"
        ];
        $response = $this->post(route('get_xml_data'),$formData);
        $response->assertStatus(200);
    }    

    /**
     * Only test if no post data.
     *
     * @return void
     */
    public function test_can_retrieve_xml_by_blank()
    {
        $formData = [];
        $response = $this->post(route('get_xml_data'),$formData);
        $response->assertStatus(200);
    }    
}
